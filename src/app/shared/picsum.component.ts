import { booleanAttribute, Component, Input, numberAttribute } from '@angular/core';

@Component({
  selector: 'app-picsum',
  standalone: true,
  imports: [],
  template: `
    <img 
      [src]="url"
      [alt]="alt"
    >
  `,
  styles: ``
})
export class PicsumComponent {
 /* @Input({ alias: 'width', transform: numberAttribute })
  w: number = 300;

  @Input({ transform: numberAttribute })
  h: number = 200;
*/
  @Input({ required: true }) alt: string | undefined;

  @Input({ transform: booleanAttribute })
  grayscale: boolean = false;

  @Input({ transform: (val: 'sm' | 'xl') => {
      switch (val) {
        case 'sm': return 400
        case 'xl': return 500
        default:
          return 300
      }
  }})
  size: number = 0;

  url: string = ''

  ngOnChanges() {
    // this.url = 'https://picsum.photos/' + this.size + '/' + this.size
    this.url =  this.grayscale ?
      `https://picsum.photos/${this.size}/${this.size}?grayscale`  :
      `https://picsum.photos/${this.size}/${this.size}`
  }

}
