import { NgClass } from '@angular/common';
import { Component, effect, input, model } from '@angular/core';

@Component({
  selector: 'app-side-panel',
  standalone: true,
  imports: [
    NgClass
  ],
  template: `
    <div 
      class="fixed p-3 bg-white dark:bg-slate-900 w-96  top-0 bottom-0 z-40 border-l-2 border-sky-400 transition-all duration-1000"
      [ngClass]="{
        '-right-96': !opened(),
        'right-0': opened()
      }"
    >
      <h1>{{title()}}</h1>
      <button (click)="opened.set(false)">❌</button>
      <hr>
      <ng-content></ng-content>
    </div>
  `,
  styles: ``
})
export class SidePanelComponent {
  title = input.required<string>()
  opened = model.required()

  constructor() {
    effect(() => {
      console.log(this.title())
    });
  }
}
