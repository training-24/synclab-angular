import { booleanAttribute, Component, computed, input, Input, numberAttribute } from '@angular/core';

@Component({
  selector: 'app-picsum-signal',
  standalone: true,
  imports: [],
  template: `
    <img [src]="url()" >
  `,
  styles: ``
})
export class PicsumSignalComponent {
  w = input(300, { transform: numberAttribute })
  h = input(300, { transform: numberAttribute })
  grayscale = input(false, { transform: booleanAttribute } )

  url = computed(() => {
    const w = this.w()
    const h = this.h()
    const grayscale = this.grayscale() ? '?grayscale' : ''
    return `https://picsum.photos/${w}/${h}${grayscale}`
  })
}
