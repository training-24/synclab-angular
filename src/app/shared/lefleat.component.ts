import { Component, effect, ElementRef, input, untracked, viewChild } from '@angular/core';

import L, { LatLngExpression } from 'leaflet';

@Component({
  selector: 'app-lefleat',
  standalone: true,
  imports: [],
  template: `
    <div id="map" #mapRef></div>
  `,
  styles: `
    #map { height: 180px; }
  `
})
export class LefleatComponent {
  coords = input.required<LatLngExpression>()
  zoom = input<number>(8)
  mapReference = viewChild.required<ElementRef<HTMLDivElement>>('mapRef')
  map: L.Map | undefined;

  constructor() {
    effect(() => {
      this.map = L.map(this.mapReference().nativeElement)
        .setView(untracked(this.coords), untracked(this.zoom));

      L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
        maxZoom: 19,
        attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
      }).addTo(this.map);
    });

    effect(() => {
      this.map?.setZoom(this.zoom())
    });

    effect(() => {
      this.map?.setView(this.coords())
    });

  }


}
