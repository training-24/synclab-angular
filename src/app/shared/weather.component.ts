import { JsonPipe } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Component, computed, effect, inject, input, signal } from '@angular/core';

@Component({
  selector: 'app-weather',
  standalone: true,
  imports: [
    JsonPipe
  ],
  template: `
    @if (meteo(); as mt) {
      {{ temperature() }}
      {{ desc() }}
    }
  `,
})
export class WeatherComponent {
  http = inject(HttpClient)
  city = input<string>()
  meteo = signal<Meteo | null>(null)

  temperature = computed(() => `${this.meteo()?.main.temp} °`)
  desc = computed(() => `${this.meteo()?.weather[0].description}`)

  constructor() {
    effect(() => {
      this.http.get<Meteo>(`https://api.openweathermap.org/data/2.5/weather?q=${this.city()}&units=metric&APPID=eb03b1f5e5afb5f4a4edb40c1ef2f534`)
        .subscribe(res => {
          this.meteo.set(res);
        })
    });
  }
}






export interface Meteo {
  coord: Coord;
  weather: Weather[];
  base: string;
  main: Main;
  visibility: number;
  wind: Wind;
  clouds: Clouds;
  dt: number;
  sys: Sys;
  timezone: number;
  id: number;
  name: string;
  cod: number;
}

export interface Sys {
  type: number;
  id: number;
  country: string;
  sunrise: number;
  sunset: number;
}

export interface Clouds {
  all: number;
}

export interface Wind {
  speed: number;
  deg: number;
}

export interface Main {
  temp: number;
  feels_like: number;
  temp_min: number;
  temp_max: number;
  pressure: number;
  humidity: number;
}

export interface Weather {
  id: number;
  main: string;
  description: string;
  icon: string;
}

export interface Coord {
  lon: number;
  lat: number;
}
