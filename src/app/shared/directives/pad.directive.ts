import { Directive, effect, ElementRef, HostBinding, inject, input, Renderer2 } from '@angular/core';

@Directive({
  selector: '[appPad]',
  standalone: true
})
export class PadDirective {
  appPad = input<'sm' | 'lg'>('sm');

  el = inject(ElementRef<HTMLElement>)
  renderer = inject(Renderer2)

  constructor() {
    effect(() => {
      this.renderer.setStyle(
        this.el.nativeElement,
        'padding',
        this.appPad() === 'sm' ? '10px' : '100px'
      )
    });
  }

  ngOnDestroy() {

  }
}
