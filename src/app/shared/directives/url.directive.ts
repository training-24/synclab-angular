import { Directive, HostBinding, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[appUrl]',
  standalone: true
})
export class UrlDirective {
  @Input() appUrl: string | undefined
  @HostBinding() class = 'underline text-orange-400 cursor-pointer'

  @HostListener('click')
  clickMe() {
    window.open(this.appUrl)
  }

}
