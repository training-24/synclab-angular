import { Directive, effect, HostBinding, inject, input, TemplateRef, ViewContainerRef } from '@angular/core';
import { distinctUntilChanged } from 'rxjs';
import { AuthService } from '../../core/auth.service';

@Directive({
  selector: '[appIfLogged]',
  standalone: true
})
export class IfLoggedDirective {
  requiredRoles = input('', { alias: 'appIfLogged'})

  authSrv = inject(AuthService)
  tpl = inject(TemplateRef)
  view = inject(ViewContainerRef)

  constructor() {
    effect(() => {
      if (
        this.authSrv.isLogged() &&
        this.authSrv.role() === this.requiredRoles()
      ) {
        this.view.createEmbeddedView(this.tpl)
      } else {
        this.view.clear()
      }
    });

/*
    this.authSrv.isLogged$
      .pipe(
        distinctUntilChanged()
      )
      .subscribe(isLOgged => {
        console.log(isLOgged)
        if (isLOgged) {
          this.view.createEmbeddedView(this.tpl)
        } else {
          this.view.clear()
        }
      })
    */
  }


}
