import { Directive, HostBinding, input, signal } from '@angular/core';

@Directive({
  selector: '[inputBordered]',
  standalone: true
})
export class BorderedDirective {
  isBordered = input<'primary' | 'alert'>('primary',{alias: 'inputBordered'})

  // @HostBinding() hidden = true
  @HostBinding() get class() {
    let cls = ''
    switch (this.isBordered()) {
      case 'alert':
        cls =  'bg-red-500'
        break;
      default:
        cls = 'bg-blue-500'
    }

    return `input input-bordered ${cls} m-2`
  }
  @HostBinding() title = 'campo di input'
}

