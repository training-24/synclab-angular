import { JsonPipe, NgIf } from '@angular/common';
import { Component, computed, effect, EventEmitter, input, Input, model, Output } from '@angular/core';

@Component({
  selector: 'app-card',
  standalone: true,
  imports: [
    JsonPipe,
    NgIf
  ],
  template: `
    <div class="card w-full bg-base-100 shadow-xl">
      <figure><img class="w-full" src="https://daisyui.com/images/stock/photo-1606107557195-0e29a4b5b4aa.jpg" alt="Shoes" /></figure>
      <div class="card-body">
        <h2 class="card-title">
          {{ title }}
        </h2>
        
        <div>
          <ng-content></ng-content>
        </div>
        
        @if(buttonLabel) {
          <div class="card-actions justify-end">
            <button 
              (click)="buttonClick.emit()" 
              class="btn btn-primary"
            >
              {{ buttonLabel }}
            </button>
          </div>  
        }
      </div>
    </div>
 `,
})
export class CardComponent {
  @Input() title = ''
  @Input() buttonLabel: string | undefined
  @Output() buttonClick = new EventEmitter()

}
