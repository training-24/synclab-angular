import { Component, model, signal } from '@angular/core';
import { CardComponent } from '../shared/card.component';
import { PicsumSignalComponent } from '../shared/picsum-signal.component';
import { PicsumComponent } from '../shared/picsum.component';
import { SidePanelComponent } from '../shared/side-panel.component';
import { WeatherComponent } from '../shared/weather.component';

@Component({
  selector: 'app-demo7',
  standalone: true,
  imports: [
    CardComponent,
    PicsumComponent,
    PicsumSignalComponent,
    WeatherComponent,
    SidePanelComponent
  ],
  template: `
    <h1>UI Kit</h1>
    
    <pre>{{isOpen}}</pre>
    <app-side-panel
      title="DRAWER"
      [(opened)]="isOpen"
    >
      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequatur, laudantium non! Adipisci commodi corporis delectus dolor dolorum error eveniet hic impedit libero maiores, nobis, sint tempora ullam. Explicabo saepe, veniam.
      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequatur, laudantium non! Adipisci commodi corporis delectus dolor dolorum error eveniet hic impedit libero maiores, nobis, sint tempora ullam. Explicabo saepe, veniam.
      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequatur, laudantium non! Adipisci commodi corporis delectus dolor dolorum error eveniet hic impedit libero maiores, nobis, sint tempora ullam. Explicabo saepe, veniam.
      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequatur, laudantium non! Adipisci commodi corporis delectus dolor dolorum error eveniet hic impedit libero maiores, nobis, sint tempora ullam. Explicabo saepe, veniam.
    </app-side-panel>
    
    
    <button (click)="isOpen = !isOpen">Open Drawer</button>

    <hr>
    
    
    <h1>Weather</h1>
    <app-weather city="Milan" />

    <h1>Picsum</h1>
    <app-picsum-signal
      w="400" h="300"
      grayscale
    />
    
    <!--<app-picsum alt="ciao" size="sm"  />
    <app-picsum alt="miao" size="xl" grayscale  />
    -->
    
    
    <div class="flex gap-3">
      <div class="w-1/2">
        <app-card 
          title="ONE" 
          buttonLabel="VISIT URL"
          (buttonClick)="doSomething()"
        >
          Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi dicta dolorem, ex excepturi fugiat illo maiores numquam placeat quam quisquam ullam ut velit vitae! Aliquid aperiam eveniet exercitationem explicabo quas?
        </app-card>
      </div>
      <div class="w-1/2">
        <app-card title="TWO">
          <input type="text" class="input input-bordered">
          <input type="text" class="input input-bordered">
          <input type="text" class="input input-bordered">
        </app-card>
      </div>
    </div>


    <button class="my-btn">CLICK</button>
    <button class="my-btn">CLICK</button>
    <button class="my-btn">CLICK</button>
    
    <input type="text" class="input input-bordered" >
  `,
  styles: `
   
  `
})
export default class Demo7Component {

  isOpen = false;
  doSomething() {
    window.alert('hello!')
  }
}
