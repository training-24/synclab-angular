import { AsyncPipe, JsonPipe } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Component, computed, inject, signal } from '@angular/core';
import { toSignal } from '@angular/core/rxjs-interop';
import { interval, map } from 'rxjs';
import { UsersService } from '../../core/users.service';
import { User } from '../../model/user';

@Component({
  selector: 'app-demo3',
  standalone: true,
  imports: [
    JsonPipe,
    AsyncPipe
  ],
  template: `
    <h1>Obs + toSignal</h1> 
    total items: {{ totalItems() }}
    @for (user of users(); track user.id) {
      <li>{{ user.name }} - {{ user.id }}</li>
    }
    <hr>
    € {{totalCosts()}}
  `,
  providers: [
    UsersService,
  ]
})
export default class Demo3Component {
  users = toSignal(
    inject(HttpClient).get<User[]>('https://jsonplaceholder.typicode.com/users')
  )

  totalItems = computed(() => this.users()?.length);

  totalCosts = computed(() => this.users()?.reduce((acc, item) => {
    return acc + item.id
  }, 0))

}
