import { Component, signal } from '@angular/core';
import { CardComponent } from '../shared/card.component';

@Component({
  selector: 'app-demo4',
  standalone: true,
  imports: [
    CardComponent
  ],
  template: `
    <h1>Defer Example</h1>

    @defer (on interaction) {
      <app-card title="blabla"/>
    } @placeholder {
      <button>Load List</button>
    } @loading {
      <div>Rendering ...</div>
    } @error {
      <div>Failed to load chunk</div>
    }
  `,
  styles: ``
})
export default class Demo4Component {

}
