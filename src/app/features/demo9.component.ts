import { NgClass } from '@angular/common';
import { AfterViewInit, Component, effect, ElementRef, viewChild, ViewChild } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterLink } from '@angular/router';
import { CardComponent } from '../shared/card.component';
import { BorderedDirective } from '../shared/directives/bordered.directive';
import { PadDirective } from '../shared/directives/pad.directive';
import { UrlDirective } from '../shared/directives/url.directive';

@Component({
  selector: 'app-demo9',
  standalone: true,
  imports: [
    CardComponent,
    RouterLink,
    NgClass,
    BorderedDirective,
    PadDirective,
    FormsModule,
    UrlDirective
  ],
  template: `

    <div appUrl="https://www.google.com">go to google</div> bla bla 
    <input type="text" [(ngModel)]="value">
    <span [appPad]="value">♥️</span>
    <span appPad="lg">💩</span>
    
    <input appPad="sm" inputBordered="primary" placeholder="hello" />    
    <input inputBordered="alert"
           placeholder="hello" />    

    <hr>
    <input type="text" #inputRef>
    <app-card title="pippo"/>
    <button (click)="doSomething(inputRef)">CLICK</button>
  `,
  styles: ``
})
export default class Demo9Component {
  value: 'sm' | 'lg' = 'sm';

  input = viewChild<ElementRef<HTMLInputElement>>('inputRef')
  card = viewChild.required(CardComponent)

  constructor() {
    effect(() => {
      this.input()?.nativeElement.focus()
      /*console.log(this.input()?.nativeElement)
      console.log(this.card().title)*/
    });
  }

  doSomething(input: HTMLInputElement) {
    console.log(input.value)
  }
}
