import { AsyncPipe, JsonPipe, NgIf } from '@angular/common';
import { Component, effect, inject, TemplateRef, viewChild, ViewContainerRef } from '@angular/core';
import { AuthService } from '../core/auth.service';

@Component({
  selector: 'app-demo6',
  standalone: true,
  imports: [
    JsonPipe,
    AsyncPipe,
    NgIf
  ],
  template: `


    <ng-template #tpl>
      CIAO
    </ng-template>
    
    
    <h1>Login Simulation</h1>
    <h1>{{ authSrv.auth$ | async | json }}</h1>
    <h1>--> {{ authSrv.username$() | async }}</h1>
    
    <button (click)="authSrv.signIn()">SignIn</button>
    
    
  `,
  styles: ``
})
export default class Demo6Component {
  authSrv = inject(AuthService)


}
