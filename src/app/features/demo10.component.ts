import { Component } from '@angular/core';
import { LefleatComponent } from '../shared/lefleat.component';

@Component({
  selector: 'app-demo10',
  standalone: true,
  imports: [
    LefleatComponent
  ],
  template: `
    <p>
      demo10 works!
    </p>

    <div>
      <button (click)="zoom = zoom - 1">-</button>
      <button (click)="zoom = zoom + 1">+</button>
      <button (click)="coords = [44, 12]">Coords 1</button>
      <button (click)="coords = [42, 11]">Coords 2</button>
      <button (click)="coords = [45, 10]">Coords 3</button>
    </div>

    <app-lefleat [coords]="coords" [zoom]="zoom"/>
    <app-lefleat [coords]="[31, 13]"  />
   
  `,
  styles: ``
})
export default class Demo10Component {
  zoom = 6;
  coords: [number, number] = [43, 13];

}
