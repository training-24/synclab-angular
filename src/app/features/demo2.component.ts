import { log } from '@angular-devkit/build-angular/src/builders/ssr-dev-server';
import { JsonPipe } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Component, computed, inject, signal } from '@angular/core';
import { User } from '../model/user';

@Component({
  selector: 'app-demo2',
  standalone: true,
  imports: [
    JsonPipe
  ],
  template: `
    <h1>Crud Example</h1>
    <input type="text" (keydown.enter)="addUser(inputRef)" #inputRef>
    
    {{totalItems()}} users
    @for(user of users(); track user.id) {
      <li>
        {{$index+1}} {{user.name}}
        <button (click)="deleteUser(user)">Delete</button>
        @if($last) {
          <hr>
        }
      </li>
    } @empty {
      <div>no items</div>
    }  
    
    TOTAL COST: {{totalCosts()}}
  `,
})
export default class Demo2Component {
  http = inject(HttpClient)
  users =  signal<User[]>([])


  totalItems = computed(() => this.users()?.length);

  totalCosts = computed(() => this.users()?.reduce((acc, item) => {
    return acc + item.id
  }, 0))

  constructor() {
    this.http.get<User[]>('https://jsonplaceholder.typicode.com/users')
      .subscribe(res => {
        this.users.set(res);
      })
  }

  addUser(input: HTMLInputElement) {
    this.http.post<User>(`https://jsonplaceholder.typicode.com/users`, {
      name: input.value
    })
      .subscribe((newUser) => {
        this.users.update(
          users => [...users, newUser]
        )
      })
  }


  deleteUser(userToRemove: User) {
    this.http.delete(`https://jsonplaceholder.typicode.com/users/${userToRemove.id}`)
      .subscribe(() => {
        this.users.update(
          users => users.filter(u => u.id !== userToRemove.id)
        )
      })
  }
}
