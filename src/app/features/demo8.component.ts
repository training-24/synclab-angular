import { HttpClient } from '@angular/common/http';
import { Component, effect, inject, input, Input, signal } from '@angular/core';
import { RouterLink } from '@angular/router';

@Component({
  selector: 'app-demo8',
  standalone: true,
  imports: [
    RouterLink
  ],
  template: `
    <h1>ROUTE PARAMS </h1>
    <p>
      ID: {{id()}} - TITLE: {{title()}}
    </p>
    
    <button routerLink="/demo8/123">123</button>
    <button routerLink="/demo8/456">456</button>
  `,
  styles: ``
})
export default class Demo8Component {
  title = input<string | undefined>('')
  id = input<string | undefined>('')
  http = inject(HttpClient)

  constructor() {
    effect(() => {
      console.log('http...', this.id())
    });
  }
}
