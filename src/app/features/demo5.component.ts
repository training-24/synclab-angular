import { AsyncPipe, JsonPipe, NgIf } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Component, computed, inject, untracked } from '@angular/core';
import { takeUntilDestroyed, toSignal } from '@angular/core/rxjs-interop';
import { FormControl, ReactiveFormsModule } from '@angular/forms';
import { ActivatedRoute, RouterLink } from '@angular/router';
import {
  concatMap,
  debounceTime,
  delay, distinctUntilChanged,
  exhaustMap,
  interval,
  map,
  mergeAll,
  mergeMap,
  of, share,
  switchMap,
  take,
  tap
} from 'rxjs';
import { User } from '../model/user';

@Component({
  selector: 'app-demo5',
  standalone: true,
  imports: [
    RouterLink,
    ReactiveFormsModule,
    AsyncPipe,
    JsonPipe,
    NgIf
  ],
  template: `
    <h1>Params, RxJS, Forms</h1>
    <div *ngIf="user$ | async as user">
      {{ user.name }} 
    </div>
    @if(user$ | async; as user) {
      {{ user.name }} 
    }

    <hr>
    <button routerLink="../1">1</button>
    <button routerLink="/demo5/2">2</button>
    <button routerLink="/demo5/3">3</button>

    <div>
      {{ (user$ | async)?.username }}
    </div>
    
    
    <input type="text" [formControl]="input">

    -->  {{isEmpty() | json}}
    @if (!isEmpty() ) {
      <h1>{{usersSignal()}}</h1>
      <pre>{{ totalUsers() }} users</pre>  
    }
    
  `,
  styles: ``
})
export default class Demo5Component {
  activateRouted = inject(ActivatedRoute)
  http = inject(HttpClient)

  timer = interval(1000)
    .pipe(share(), take(2))

  input = new FormControl()

  user$ = this.activateRouted.params
    .pipe(
      takeUntilDestroyed(),
      share(),
      map(params => params['id']),
      mergeMap(userId => this.http.get<User>(`https://jsonplaceholder.typicode.com/users/${userId}`)),
    )

  users$ = this.input.valueChanges
    .pipe(
      debounceTime(700),
      distinctUntilChanged(),
      mergeMap(text => this.http.get<User[]>(`https://jsonplaceholder.typicode.com/users?q=${text}`)),
    );


  isEmpty = computed(() => {
    if (this.usersSignal()?.length) {
      return !this.usersSignal()?.length;
    }
    return true
  })
  totalUsers = computed(() => {
    return this.usersSignal()?.length
  })

  usersSignal = toSignal(
    this.input.valueChanges
      .pipe(
        debounceTime(700),
        distinctUntilChanged(),
        mergeMap(text => this.http.get<User[]>(`https://jsonplaceholder.typicode.com/users?q=${text}`)),
      )
  )
}

