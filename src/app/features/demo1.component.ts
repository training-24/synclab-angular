import { NgIf } from '@angular/common';
import { Component, computed, effect, signal } from '@angular/core';
import { FormsModule } from '@angular/forms';

@Component({
  selector: 'app-demo1',
  standalone: true,
  imports: [
    FormsModule,
    NgIf
  ],
  template: `
    
    <h1>Signal and Effect demo</h1>
    <p [style.color]="isZeroColor()">
      Counter {{counter()}} 
    </p>

    
    <button (click)="dec()">-</button>
    <button (click)="inc()">+</button>
    <button (click)="reset()">reset</button>
    
    
    <button (click)="doSomething()">Do Something</button>
  `,
  styles: ``
})
export default class Demo1Component {
  counter = signal(1)

  constructor() {
    effect(() => {
      localStorage.setItem('counter', this.counter().toString())
    });
  }

  isZero = computed(() => {
    return this.counter() === 0
  });

  isZeroColor = computed(() => {
    return this.isZero() ? 'red' : 'green'
  })

  dec() {
    this.counter.update((prev) => prev - 1)
  }

  inc() {
    this.counter.set(this.counter() + 1)
  }

  reset() {
    this.counter.set(0)
  }


  doSomething() {
    // console.log('doSomething')
  }

}

