import { Routes } from '@angular/router';

export const routes: Routes = [
  { path: 'demo1', loadComponent: () => import('./features/demo1.component') },
  { path: 'demo2', loadComponent: () => import('./features/demo2.component') },
  { path: 'demo3', loadComponent: () => import('./features/demo3/demo3.component') },
  { path: 'demo4', loadComponent: () => import('./features/demo4.component') },
  { path: 'demo5/:id', loadComponent: () => import('./features/demo5.component') },
  { path: 'demo6', loadComponent: () => import('./features/demo6.component') },
  { path: 'demo7', loadComponent: () => import('./features/demo7.component') },
  {
    path: 'demo8/:id',
    loadComponent: () => import('./features/demo8.component'),
    data: { title: 'custom title'}
  },
  { path: 'demo9', loadComponent: () => import('./features/demo9.component') },
  { path: 'demo10', loadComponent: () => import('./features/demo10.component') },
  { path: '', redirectTo: 'demo1', pathMatch: 'full'}
];
