import { Component } from '@angular/core';
import { RouterLink, RouterOutlet } from '@angular/router';
import { NavbarComponent } from './core/navbar.component';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [RouterOutlet, NavbarComponent],
  template: `
    <app-navbar />
    
    <hr>
    
    <div class=" max-w-screen-sm mx-6 xl:mx-auto mt-6 ">
      <router-outlet />
    </div>
  `,
})
export class AppComponent {
}
