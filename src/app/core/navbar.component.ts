import { AsyncPipe, JsonPipe } from '@angular/common';
import { ChangeDetectionStrategy, Component, computed, inject } from '@angular/core';
import { RouterLink } from '@angular/router';
import { IfLoggedDirective } from '../shared/directives/if-logged.directive';
import { AuthService } from './auth.service';

@Component({
  selector: 'app-navbar',
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [
    RouterLink,
    AsyncPipe,
    JsonPipe,
    IfLoggedDirective
  ],
  template: `
    <div class="flex gap-2 flex-wrap">
      <button class="my-btn" routerLink="demo1">demo1</button>
      <button routerLink="demo2">demo2</button>
      <button routerLink="demo3">demo3</button>
      <button routerLink="demo4">demo4</button>
      <button routerLink="demo5/1">demo5</button>
      <button routerLink="demo6">demo6</button>
      <button routerLink="demo7">demo7</button>
      <button routerLink="demo8/123">demo8</button>
      <button routerLink="demo9">demo9</button>
      <button routerLink="demo10">demo10</button>

      @if (authSrv.isLogged()) {
        <button (click)="authSrv.logout()">logout 1</button>
      }

      <button 
        *appIfLogged="'admin'" 
        (click)="authSrv.logout()">logout 2</button>

      
      {{ authSrv.usernameSignal()}}
    </div>

  `,
  styles: ``
})
export class NavbarComponent {
  authSrv = inject(AuthService)

}
