import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ThemeService {
  theme = 'dark'

  changeTheme(val: string) {
    this.theme = val;
  }
}
