import { computed, Injectable, signal } from '@angular/core';
import { BehaviorSubject, map, Observable } from 'rxjs';


interface Auth {
  token: string;
  role: string;
  name: string;
}

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  auth: Auth | null = null;
  auth$ = new BehaviorSubject<Auth | null>(null)
  authSignal = signal<Auth | null>(null)

  usernameSignal = computed(() => {
    return this.authSignal()?.name.toUpperCase() + ' ' + this.authSignal()?.role
  })

  isLogged = computed(() => {
    return !!this.authSignal()
  })

  role = computed(() => {
    return this.authSignal()?.role
  })

  signIn() {
    // http.get
    const res: Auth = { token: 'abc', role: 'admin', name: 'fabio'}
    this.auth = res;
    this.auth$.next(res)
    this.authSignal.set(res)
  }

  logout() {
    this.auth = null;
    this.auth$.next(null)
    this.authSignal.set(null)
  }



  get isLogged$(): Observable<boolean> {
    return this.auth$
      .pipe(
        map(data => !!data)
      )
  }

  username$() {
    return this.auth$
      .pipe(
        map(auth => auth?.name)
      )
  }

}
